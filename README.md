# IETI OS

![alt text](imatges/ietiOS_escriptori.png "foto1"){width=1024 height=576px}


Benvinguts la repositori de IETI OS, la distribució basada en Arch de l'institut Esteve Terradas i Illa (d'ara endavant, IETI)


super::superlocal

alumne::alumne

manteniment:: *********

Imatges de disc oficials:

IETI_OS_1_0 --> https://drive.google.com/file/d/1nqNt2boL1e60wq2ZU8pA5YPaSiKbODOg/view?usp=sharing

10 de Març de 2025

# Introducció

Aquesta personalització de Arch Linux està pensada per poder treballar als cicles formatius d'informàtica que impartim a IETI. Té el software mínim que fem servir (després ampliable segon les necessitats del professorat/alumnat).

La distro és un Archlinux base amb KDE. Tota la informació sobre Arch es troba a la seva wiki

https://wiki.archlinux.org/

Arch és un linux "rolling release", això vol dir que no canvia de versió, simplement actualitzem el que fem servir i el kernel.

El kernel que he escollit per ARCHIETI no és el kernel estàndar si no el linux_zen kernel, està "tunejat" per treballar millor amb gràfics entre altres avantatges.

En quant a la filosofia d'Arch Linux...

Arch es basa en el principi KISS

                Keep it simple, stupid!

La idea es mantenir el sistema el més simple i lleuger possible. 

# Escriptori

He escollit l'escriptori KDE (Versió 6, estrenada al març del 2024). L'he escollit per la seva capacitat de personalització principalment. En cas de necessitar altres escriptoris sempre es podrien afegir.

https://kde.org/es/announcements/megarelease/6/


# Sistema de fitxers BTRFS

El sistema de fitxers que utilitza la IETI OS és BTRFS (B-tree filesystem). És un sistema de fitxers modern, ja considerat estable basat en el copy-on-write. L'avantatge principal d'aquest sistema és la possibilitat de fer "snapshots" del sistema per poder restaurar qualsevol possible errada de l'usuari.

# Snapshots (opcional)
 
 El sistema d'snapshots està configurat amb snapper + btrfs assistant (script Configurar_snapshots.sh)

 he configurat una snapshot principal just el moment després de configurar tot el software (en qualsevol moment és pot fer alguna altra snapshot manual) i també he fet que es generin una snapshot pre i post qualsevol canvi que es faci al sistema (fins a un màxim de 10, es van autonetejant). 

 També està configurat per tal de que la carpeta de snapshots es carregui al propi GRUB del sistema per si IETI OS no és accessible per l'usuari. Des del GRUB podriem entrar en una de les imatges del sistema que funcionesin correctament i restaurar a un punt correcte. 

# Instal·lació, actualització i manteniment de paquets

El gestor de paquets nadiu de archlinux és "pacman". 

- Per instal·lar un paquet del repositori fariem:

        sudo pacman -S firefox

- Per desinstal·lar un paquet farem:

        sudo pacman -R firefox
        
- Per actualitzar tots els paquets farem:

        sudo pacman -Syu

Però un dels principals avantatges de arch és poder fer servir el AUR (arch user repository)

https://aur.archlinux.org/

Aquí es troben moltíssimes aplicacions i eines preparades per instal·lar a arch. La manera estàndar d'instal·lar les aplicacions de AUR és clonen la direccio del repositori i fent el build dle paquet. Per evitar això he configurat "yay" (script 1.install_yay.sh). Yay és un "ajudant" que ens facilitarà treballar amb AUR ja que automatitzarà la clonació del repositori i la seva construcció.

- Volem instal·lar "Android Studio", que no està a la maqueta base i no està al repositori oficial només hem de fer els següents passos:

        1- Buscar a AUR si existeix (https://aur.archlinux.org/packages/android-studio)
        2- yay -S android-studio

- En cas de voler desinstal·lar una aplicació de AUR es farà

        yay -R <nom del paquet>

# Flatpak

He afegit que de sèrie IETI OS accepti paquets flatpak, els podeu instal·lar des de flathub https://flathub.org/ o des de la botiga d'aplicacions que teniu instal·lada "Discover"

# Sobre les actualitzacions...

Per actualitzar les aplicacions amb pacman ja ho hem vist, però no actualitzarà les aplicacions de AUR ni les flatpak.

Quan tenim aplicacions de diferents tipus, el correcte és fer servir yay per actualitzar.

Us recomano actualitzar sempre amb:

        yay -Syu
